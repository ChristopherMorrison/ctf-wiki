# What is a Program?
Although we should already be aware of this, it is important to remember the
average computer program relies on a lot of other things just to be able to
print "Hello World".

Consider for example, watching a movie on DVD. We don't just stare at the DVD
and have the movie visible on the disk. We have to put the DVD into a player,
which in turn needs to be connected a screen. The DVD doesn't know about the
screen we're using or even what size it is, the player takes care of talking to
the screen for us. The player just reads what the DVD has on it and then
interacts with the hardware like the screen, speakers, and remote.

```
                Screen ◄──────► Player ◄──────► DVD
```

In this example our DVD is just the same as an average (user space) program. The
player represents our underling operating system which gives us the very simple
interface of the disk tray to put the DVD in and watch it. In computers this is
just the same concept as running a program. Then our operating system, like the
player, maintains the abstraction and interfacing details of the hardware so
that our program doesn't need to know about specific hardware features. When our
program runs, it runs in a simulated version of "the computer" where it is able
to talk to the kernel, and on behalf of the program, the kernel is able to talk
with the hardware.


```
                Hardware ◄──────► Kernel ◄──────► Program
```

Of course we can run more than one program at a time on a modern computer,
unlike the DVD example. Typically programs are organized into tree heirarchies
in modern operating systems, with the concept of child and parent programs. We
call this the process tree.

```
                Hardware ◄──────► Kernel ◄──────► Program
                                                    |Program
                                                    |   |Program
                                                    |Program
```
