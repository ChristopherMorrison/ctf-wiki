# Reverse Engineering Overview
Reverse engineering is the practice of analyzing existing programs and systems
in order to figure out how they behave. Traditionally this means starting with
C reverse engineering.


## Why is RE always in C?
This is a classic question that a lot of people beginning in RE ask. There's not
really a straight forward answer to this. It becomes self evident to those who
are experienced that C is a natural starting point, and I'll try to explain this
in an intro section. The short of it is that while C seems like a harder
language, practically C has little abstraction from the operating system. This
means when we learn the basics of how C actually works, then we arrive at a point
in which we can only build up as there is nothing (besides the physical
implementation of the machine) underneath.

Other languages, such as document embedded scripting languages, may look like
the more simple targets. But for a variety of reasons, these are actually more
complicated as they will make use of techniques that in order to properly
understand, we will want to properly understand the properties of how C and
assembly programs work as a running process.

## So where do we start?
This section will kick off with a little operating systems review, or
potentially a new lesson for those who are seeing it for the first time. We'll
start with a C program and strip away the few layers beneath it. From this we'll
have not only an understanding of how C works, but a place to start when
describing more complex languages, runtimes, and even shellcode.
