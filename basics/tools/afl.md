# AFL
## First Rule of AFL, don't use AFL
Use aflplusplus, it's maintained and developed

## Get AFL++
This requires a sane amount of C/Cpp tooling like git/make/gcc
```
git clone https://github.com/aflplusplus/aflplusplus
cd aflplusplus
make
```

You should now have the core tools:
| tool          | description |
|---------------|-------------|
| afl-fuzz      | This fuzzes
| afl-cmin      | Reduce results to unique crashes
| afl-tmin      | Reduce result to smallest size

And the compilers:
| tool          | description |
|---------------|-------------|
| afl-as        | instrumentation compiler (asm)
| afl-cc        | instrumentation compiler (c/c++)
| afl-c++       | link to afl-cc
| afl-gcc       | link to afl-cc
| afl-g++       | link to afl-cc
| afl-clang     | link to afl-cc
| afl-clang++   | link to afl-cc

Noticing a pattern with the compilers?

Performance tools:
| tool          | description |
|---------------|-------------|
| afl-gotcpu    | checks cpu utilization
| afl-plot      | plots results
| afl-persistent-config | Tweaks machine in bad ways to get more speed

Other tools:
| tool          | description |
|---------------|-------------|
| afl-analyze   | infer input file syntax (ie basic struct fields) (ie basic struct fields)
| afl-showmap   | show complex coverage information that doesn't make sense

There's other stuff too but it's mainly internal sub-tools

## I NEED TO FUZZ
Woah there partner, better slow down and learn the rules

1. Identify a target
    - If you're starting, use a ctf bin that reads from stdin (not argv)
    - If you're reading this for advanced, find a specific asm subroutine and try that
2. Identify a way to call the target (usually just running it)
3. fuzz!
4. FUZZ MORE!
5. Crash corupus minification and triage
6. goto 1

## Simple Fuzzing Target
With the `ctf-wiki/pwn/examples/ret2win` binary

Afl++ with the `-n` flag for no instrumentation (we'll get to that later), crashes identified as real crashes of the binary called via a fork+exec loop
```
# Make an initial input
$ mkdir in
$ echo initial > in/initial
# fuzz it
$ afl-fuzz -n -i in/ -o out/ -- ./ret2win
```

The afl dashboard should pop up and crashes should be fast for this one. I let it run for about 1 min and got well over 1500 crashes.

This lets us find crashes automatically but without any instrumentation to introspect the binary, we can't tell do any afl triage on these crashes other than manual analysis.

## Building AFL++ qemu_mode
Qemu mode will be able to intercept instructions like `call` to infer the coverage graph of our binary and let us explore crashes more efficiently. qemu mode can also do other things but with a raw bianry this will be easiest. An alternative would be to recompile the binary instrumented

```
$ cd ~/aflplusplus/qemu_mode
$ ./build_qemu_support.sh
# you will probably have to keep installing required libs until this works
```

## Crash Triage
First, let's reduce our crashes to what afl things is unique:
```
# should drastically reduce the number of crashes
$ afl-cmin -Q -C -i out/crashes -o out/crashes-min -- ./ret2win

# Now we'll grab one of those and reduce it to what afl thinks it can be reduced to
# this particular example doesn't actually reduce though
$ afl-tmin -Q -i out/crashes-cmin/id:007 -o crash.min.bin -- ./ret2win
```

Now lets do some quick high-level analysis on where the input is used
```
$ ltrace -i ./ret2win < crash.min.bin
[0x40107c] read(0, "00000000000000000000000000000000"..., 100)        = 41
[0x7fcb18eff56e] --- SIGSEGV (Segmentation fault) ---
[0xffffffffffffffff] +++ killed by SIGSEGV +++
```
So we have a clear bad read at 0x40107c


Objdump shows us:
```
000000000040105e <vuln>:
  40105e:       55                      push   rbp
  40105f:       48 89 e5                mov    rbp,rsp
  401062:       48 83 ec 20             sub    rsp,0x20
  401066:       48 8d 45 e0             lea    rax,[rbp-0x20]
  40106a:       ba 64 00 00 00          mov    edx,0x64
  40106f:       48 89 c6                mov    rsi,rax
  401072:       bf 00 00 00 00          mov    edi,0x0
  401077:       e8 a4 ff ff ff          call   401020 <read@plt> # read(0, stack_char[0x20], 0x64)
> 40107c:       90                      nop
  40107d:       c9                      leave  
  40107e:       c3                      ret

```
ltrace seemed to show us one addr after our call, manual analysis or decompiling shows this is a classic overflow read to stack. From here we can write an exploit and solve the challenge.
