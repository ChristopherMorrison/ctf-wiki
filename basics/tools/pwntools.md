# pwntools
A python toolkit and library oriented towards binary exploitation in CTF's

The toolkit has common utilities for manipulating binaries and payloads.

The library has python utilities for quickly making nice network facing scripts and using the toolbox programmatically.

https://docs.pwntools.com/en/stable/

## Installation
https://docs.pwntools.com/en/stable/install.html

```
# Might need to install some other deps. If this fails, check website
python3 -m pip install --upgrade pwntools
```

## Use Examples
The `pwn` command is the entrypoint for pwntools. It had the following sub-utiliites, all of which are also top level submodules in pwn:
```
- asm                 Assemble shellcode into bytes
- checksec            Check binary security settings
- constgrep           Looking up constants from header files. Example: constgrep -c freebsd -m ^PROT_ '3 + 4'
- cyclic              Cyclic pattern creator/finder
- debug               Debug a binary in GDB
- disasm              Disassemble bytes into text format
- disablenx           Disable NX for an ELF binary
- elfdiff             Compare two ELF files
- elfpatch            Patch an ELF file
- errno               Prints out error messages
- hex                 Hex-encodes data provided on the command line or stdin
- phd                 Pwnlib HexDump
- pwnstrip            Strip binaries for CTF usage
- scramble            Shellcode encoder
- shellcraft          Microwave shellcode -- Easy, fast and delicious
- template            Generate an exploit template
- unhex               Decodes hex-encoded data provided on the command line or via stdin.
```

We've found the go-to modules are always:
- checksec -- to determine ELF protections and viable exploit methods
- cyclic -- for calculating overflow sizes automatically
- shellcraft -- for quickly writing linear shellcode that can be platform rebased easily
- asm -- for teting shellcraft output
- errno -- for debugging linux syscall results
    - honestly this one is good for normal linux development outside of CTF's

Examples (primarily script based):
- https://docs.pwntools.com/en/stable/intro.html
- https://guyinatuxedo.github.io/02-intro_tooling/pwntools/index.html
    - quick overview of the scripting objects
- https://tc.gts3.org/cs6265/2019/tut/tut03-02-pwntools.html
    - cyclic to overflow and shell
    - shellcraft for i386 linux payload and shell
