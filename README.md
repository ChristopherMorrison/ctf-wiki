# CTF Wiki
(another) Collection of CTF related concepts and tools.

This wiki does not to be a project to re-create a complete knowledge base but should serve as a place to glob useful links and tools together.
Please prefer (reputable) open internet articles and links to writing a new page.

Please keep any non-opensource opinions and information out of this wiki, don't wany anyone to get in trouble.

Ideally each concept would have at least one of each of the following:
 - article/presentation
 - video presentation
 - List of tools that assist
 - example ctf challenges
 - additional sources for in-depth coverage
