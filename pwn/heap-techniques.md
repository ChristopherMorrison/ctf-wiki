# Heap Techniques
## Heap Metadata
## Heap Smashing
Buffer overflow but on the heap, usually results in corrupting objects or other alloc'ed memory to be used elsewhere.

Articles/Presentations:
- https://en.wikipedia.org/wiki/Heap_overflow
- https://ctf101.org/binary-exploitation/heap-exploitation/

Videos:

Examples:

## Type Confusion
- https://hackingportal.github.io/Type_Confusion/type_confusion.html

## Use-After-Free / Double Free
- https://cwe.mitre.org/data/definitions/416.html
- https://cwe.mitre.org/data/definitions/415.html
- TL:DR We can control data that the software really shouldn't be using, usually in the heap

Heap TODO
---------
- controlling malloc
- clobbering
- vtable overrides
- vtable override to iterate through rop gadgets
    - https://securitylab.github.com/research/CVE-2020-6449-exploit-chrome-uaf/
