# Shellcode
Shellcoding is the technique of crafting and injecting code into a program, to
get it to carry out actions desired by the attacker.


Generally shellcode boils down to a few steps:
 1. Crafting a target appropriate payload
 2. Injection of executable code into the program
 3. Executing the injected code
    - This might require additional exploitation techniques to discover where
      the shellcode is or make the actual shellcode memory executable.

<!-- 

Articles/Presentations:
 - https://ir0nstone.gitbook.io/notes/types/stack/shellcode
 - https://wiki.bi0s.in/pwning/stack-overflow/return-to-shellcode/
 - https://pwn.college/modules/shellcode

Videos:
 - https://www.youtube.com/playlist?list=PL-ymxv0nOtqomtHqMqqgpuvWdVSs9NCBK
-->

## Linux Shellcode: Syscalls
Linux syscalls are 'magic' functions that get the kernel to do something for a
userspace program, such as actually opening a file from a drive, and proxying
access back to our user space program. Considering that everything has to be
done thorugh the kernel at some point, these are a very compact and highly
portable way to interact with the underlying OS.

In Linux, syscalls are made by setting correct register arguments and hitting a
architecture-specific syscall instruction. For the vast majority of programs,
syscalls are simply proxied into the library runtime, which is exactly what the
C library is.

Probably the best reference on syscalls is the [chromium project's
table](https://chromium.googlesource.com/chromiumos/docs/+/master/constants/syscalls.md)
that list every major consumer architecture and what the magic numbers and
registers are for each.

For a more in depth walkthrough that covers several methods for crafting Linux
shellcode payloads, take a look at the
[shellcode-demo](https://gitlab.com/christophermorrison/shellcode-demo) project
on gitlab.

## Windows Shellcode: TIB->PEB->Loader Access
From a Windows perspective things get a little weirder. Windows does have
syscalls, similar to Linux. However unlike linux, Windows only has a stable API
and not a stable ABI, so we cannot reliably write syscall payloads for writing
highly portable shellcode payloads.

Instead we rely on using the Thread Information Block (TIB) which can be
directly accessed from the FS register. From the TIB we can access the Process
Environment Block (PEB), and from the PEB we can access the `PEB_LDR_DATA`
structure which contains a linked list of metadata for all currently loaded
DLL's. This list then lets us access core functions such as `LoadLibraryA` and
`GetProcAddress` in order to load and use any Windows library we desire.

## Example Challenge: ret2shellcode
Continuing from our simple stack problem, ret2win, we are going to make two changes:
1. The `win()` function is removed from the binary, unfortunately I often find
   that real life applications remove these more often than not.
2. We will make the program's stack memory executable, which is still the
   default behavior on many older or low end devices.

[ret2shellcode](examples/ret2shellcode/)
```c
{{#include examples/ret2shellcode/ret2shellcode.c}}
```

This means we will need to use shellcode to put our own `win()` function into
the program memory, then we will need to control the return address of the
program in order to execute our stack based shellcode. Convieniently this
program is also printing the address of the buffer on the stack.

For this exploit, it's easiest to use pwntools's shellcraft library to build a
payload that will run sh (the most literal shell code). We can also use io
parsing and payload formating utilities to handle the return address within the payload:

```py
{{#include examples/ret2shellcode/ret2shellcode.py}}
```

<!-- TODO: ## Shellcode Under Constraints
nop slides
null byte filters
emoji filters
multi-arch payloads

- shellcode decoders/unpackers
   - https://www.cs.jhu.edu/~sam/ccs243-mason.pdf
   - see defcon 27 presentation
-->

## NX and DEP
As mentioned earlier, we had to enable the executable stack for our challenge
program. This is because like stack overflows in C, stack shellcode used to be
an extremely common and stable exploit technique against many devices. To
counter this, native programs and operating systems started defaulting to having
non-executable stacks. In Linux this is called Non-eXecutable (NX) and in
Windows this is called Data Execution Prevention (DEP). Nominally these are the
same thing and the different names are just OS specific implementations.

While this may seem like the end to shellcode, like stack canaries, this
protection has work arounds. First, like stack canaries, low end or really old
devices may not properly implement NX. Enabling techniques almost identical to
those displayed here. Second, we may be able to execute functions like
VirtualProtect or mprotect to make our shellcode on the stack or elsewhere
executable and then execute it as before.
