#include <stdio.h>

/* Compile with:
 * gcc -fno-stack-protector -o visualized-overflow visualized-overflow.c
*/

/*
    This example will overflow a stack buffer and print out the impact
*/

void main()
{
    size_t top_of_stack[1] = {0};
    register size_t rbp asm("rbp");

    int overflowed_int = 0;
    char buffer[15] = {};
    int i;

    printf("i |     buffer    | overflowed_int   | rbp (prev frame)   | rbp+8 (return address) | stack arguments...\n");
    for (i = 0; i < 19; i++)
    {
        buffer[i] = 'A';
        printf(
            "%-2.2d|%-15.15s|%-4.4s = %-10.i | %llx       | %llx\n",
            i,
            buffer,
            (char *)&overflowed_int,
            overflowed_int,
            rbp,
            top_of_stack[3]);
    }
    return;
}
