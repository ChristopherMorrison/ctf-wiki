//  gcc -fno-stack-protector -no-pie
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

void win_1()
{
    puts("w");
}

void win_2()
{
    puts("i");
}

void win_3()
{
    puts("n");
}

void vuln()
{
    char buf[32];
    read(0, buf, 100);
}

void main()
{
    vuln();
    exit(0);
}
