//  gcc -nostartfiles -fno-stack-protector -no-pie
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

void win()
{
    char mesg[5] = "win";
    puts(mesg);
}

void vuln()
{
    puts("vuln");
    char buf[0x20];
    read(0, buf, 0x40);
}

void vuln2()
{
    puts("vuln2");
    char buf[0x20];
    read(0, buf, 100);
}

void _start()
{
    vuln();
    exit(0);
}
