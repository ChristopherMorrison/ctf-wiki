// gcc -nostartfiles -fno-stack-protector -z execstack
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

void vuln()
{
    char buf[256];
    printf("%p\n", buf);
    read(0, buf, 512);
}

void main()
{
    vuln();
}
