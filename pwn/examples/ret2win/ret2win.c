//  gcc -nostartfiles -fno-stack-protector -no-pie
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

void win()
{
    char mesg[4] = "win";
    puts(mesg);
}

void vuln()
{
    char buf[32];
    read(0, buf, 100);
}

void _start()
{
    vuln();
    exit(0);
}
