# Return Oriented Programming
## Thinking Past `void win(void)` Payloads
To this point, and with ret2shellcode, we have mainly explored exploit
techniques that result in being able to call an arbitrary `void func(void)`
within our program. Now we'll consider a few evolutions of these techniques that
will lead us into our next common problem categories. These concepts may seem
simple or obvious but they are critical to being able to solve non-trivial
binary exploitation problems and represent a large increase in problem
difficulty.

### Multiple return addresses in our payload.
So far, all of our exploit techniques have relied on returning into a function,
but what happens if we keep adding addresses after our target address? The
answer is that our target function will return into the next address! This
allows us to chain execution through multiple functions that may not otherwise
be in a call path.

Consider another evolution of ret2win, [multiple-returns](examples/multiple-returns/):
```c
{{#include examples/multiple-returns/multiple-returns.c}}
```

In this variation of the challenge, if we want the program to print `win` we
will need to execute multiple different functions in order. This is not
substantially different than what we have done already, requiring only that we
continue to add return addresses to the stack overflow. Tracing the stack manually will help to show this effect best.

```py
{{#include examples/multiple-returns/multiple-returns.py}}
```

### Returning into `vuln()` again for multiple payload chaining. (ret2vuln)
As mentioned above, we can just keep returning into any function we want, so why
not call our vulnerable function again? This allows us to effectively turn the
target program into a payload repl. We would do this in situations in which we
want to use one vulnerable input function to gather runtime information like
variable values and library addresses, and then use these in later payloads for
accomplishing the final goal.

Sometimes this is also seen as problems where we need to pivot from a `vuln1`
that lets us control only the return address to a `vuln2` where we can do a full
rop chain or leak other information.

Here we have a simple example, [ret2vuln](examples/ret2vuln/).
```c
{{#include examples/ret2vuln/ret2vuln.c}}
```

<!-- TODO: make this example better -->

We can modify our pwntools script to send two sequential payloads rather than a single large payload:
```py
{{#include examples/ret2vuln/ret2vuln.py}}
```

### Gadgets
So far we've just returned into the starting address of functions. This allows
us to have relatively C level behavior with the target function allocating and
deallocating its own stack then returning. However, we only really need that
these functions `ret`. Since we can adjust our 'return' address to be any
executable code, we can return to the middle of another procedure. A series of
assembly instructions ending in `ret` is generally defined as a gadget if it is
of use to us in this way.

When using gadgets, it is best to be aware of any stack cleanup that the
original procedure is expecting to do, otherwise you will end up wiping your own
stack overflow.

One of the common CTF gadgets is an otherwise unreachable call to `execve`  or
`system` that results in a shell.

[simple-gadget](/pwn/examples/simple-gadget/)
```c
{{#include examples/simple-gadget/simple-gadget.c}}
```

We can use objdump to find the setup to the execve call within the function:
```
$ objdump -d simple-gadget -M intel | grep -A 24 'impossible_win>:'
0000000000401070 <impossible_win>:
  401070:       f3 0f 1e fa             endbr64 
  401074:       55                      push   rbp
  401075:       48 89 e5                mov    rbp,rsp
  401078:       48 83 ec 10             sub    rsp,0x10
  40107c:       c7 45 fc 00 00 00 00    mov    DWORD PTR [rbp-0x4],0x0
  401083:       8b 45 fc                mov    eax,DWORD PTR [rbp-0x4]
  401086:       85 c0                   test   eax,eax
  401088:       74 3a                   je     4010c4 <impossible_win+0x54>
  40108a:       41 b8 00 00 00 00       mov    r8d,0x0; //// Execve Arg Setup Starts Here ////
  401090:       48 8d 05 69 0f 00 00    lea    rax,[rip+0xf69]        # 402000 <_start+0xf14>
  401097:       48 89 c1                mov    rcx,rax
  40109a:       48 8d 05 62 0f 00 00    lea    rax,[rip+0xf62]        # 402003 <_start+0xf17>
  4010a1:       48 89 c2                mov    rdx,rax
  4010a4:       48 8d 05 55 0f 00 00    lea    rax,[rip+0xf55]        # 402000 <_start+0xf14>
  4010ab:       48 89 c6                mov    rsi,rax
  4010ae:       48 8d 05 51 0f 00 00    lea    rax,[rip+0xf51]        # 402006 <_start+0xf1a>
  4010b5:       48 89 c7                mov    rdi,rax
  4010b8:       b8 00 00 00 00          mov    eax,0x0
  4010bd:       e8 9e ff ff ff          call   401060 <execl@plt>
  4010c2:       eb 01                   jmp    4010c5 <impossible_win+0x55>
  4010c4:       90                      nop
  4010c5:       c9                      leave  
  4010c6:       c3                      ret    
```

We plug the gadget address above into our same ret2win pwntools payload and get a shell:
```py
{{#include examples/simple-gadget/simple-gadget.py}}
```

```
$ ./simple-gadget.py 
[*] '/home/christopher/ctf-wiki/pwn/examples/simple-gadget/simple-gadget'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
[+] Starting local process '/home/christopher/ctf-wiki/pwn/examples/simple-gadget/simple-gadget': pid 43437
[*] Switching to interactive mode
$ ls
makefile  payload  simple-gadget  simple-gadget.c  simple-gadget.py
```

### Register Altering Gadgets
Now that we have gadgets, which are just useful assembly operations, we combine
their use with the data we can put on the stack. If we can use gadgets in order
to pop values off the stack into the correct registers then we can properly
format calls to functions as if we have just called them from the program
normally. This breaks us free of the `void func(void)` pattern. Worth mentioning
here is that we can use the strings from not only our target program as possible
`pop` addresses but also strings from all loaded libraries, or strings we have
managed to place into the heap as data if we can find their addresses.

It is also helpful to have a target function and its expected argument layout in
mind before looking for gadgets as it can be quickly overwhelming to start from
the gadget side. A less overwhelming approach is to write a C program as a goal,
similar to writing shellcode, and then see if it is possible to execute with a
ROP chain in the current context.

[gadget-params](examples/gadget-params/)
```c
{{#include examples/gadget-params/gadget-params.c}}
```

We can write a pwntools script that uses a rop chain to set the value of rdi to
our more desirable string in the binary and jump directly to the `system()` call:
```py
{{#include examples/gadget-params/gadget-params.py}}
```

## Full ROP
Putting it all together, we now conceptually have the ability to make an
arbitrary call to any function in our program space, limited only by the
addresses we have been able to find through leaks or analysis. In fact, it has
been proven that with enough gadgets, ROP is just as complete an execution
format as having originally written the binary.

ROP does have the obvious downside of being harder to maintain than shellcode
payloads, which can be treated as specialized C code. For this reason in many
environments it is typically more common to see ROP used as a bridge between an
exploit primitive and a mechanism to running other commands or shellcode rather
than as a full stand alone payload mechanism.

<!-- TODO: rop2shellcode example -->

## ret2libc
ret2libc is a special case of ROP that abuses the way the `system()` function is
typically implemented in libc. Typically these implementations revolve around
managing a fork and execve call to `/bin/sh` which is then relayed back to the
user code. This means we have the ideal case of `/bin/sh` and `sh` being
available strings in nearly every program memory space, allowing easy
construction of the same shell primitive we used in gadget-params.

[glibc system implementation](https://github.com/lattera/glibc/blob/master/sysdeps/posix/system.c)
```c
#define	SHELL_PATH	"/bin/sh"	/* Path of the shell.  */
#define	SHELL_NAME	"sh"		/* Name to give it.  */

<snip>

      /* Exec the shell.  */
      (void) __execve (SHELL_PATH, (char *const *) new_argv, __environ);
```

<!-- TODO: ret2libc example challenge, just do gadget-params with a libc addr leak -->

## ASLR
To cap off ROP, we must acknowledge ASLR. ASLR is Address Space Layout
Randomization, the primary defense against code reuse attacks such as ROP. The
way ASLR works is that binaries (both executables and shared libraries) that
support position-independent-code (PIC) are loaded into randomized base
addresses rather than at predictable locations.

ASLR's primary weakness as a defense is that it must be supported by both the
binary being loaded, and the loader of that binary (the kernel or the system
linker). For many older devices or software projects, one of these may not be
true, and even some default GCC outputs for lower end devices still do not
default to using PIC/PIE for all outputs, leading to predictable addresses.

Like all defenses, ASLR is not perfect. In many cases an attacker can either
predict or discover (leak) addresses of interest at runtime. For cases where
this is possible, all ASLR really does is increase the amount of effort required
to create a correct ROP chain by requiring such leaks. Some exploits may even
rely on being able to discover the address of a single object in memory and then
making deductions from that leak in order to discover other interesting
addresses.

As we have already discussed with stack frames, the return addresses are stored
on the stack, so a simple stack read primitive can get us a lot of information
on where libraries are loaded into memory. Similarly binaries will have pointers
to their dependencies in shared libraries, allowing us to potentially pivot from
one known address to others.

<!-- TODO: leak example -->


<!-- TODO: ## Stack Pivots
 - https://failingsilently.wordpress.com/2018/04/17/what-is-a-stack-pivot/
 - https://ir0nstone.gitbook.io/notes/types/stack/stack-pivoting
 - http://neilscomputerblog.blogspot.com/2012/06/stack-pivoting.html
 - https://bananamafia.dev/post/binary-rop-stackpivot/

 -->

