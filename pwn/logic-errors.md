# Memory Errors
The origin of many memory corruptions are small errors introduced by programmers
in native languages such as C or C++ which provide little  abstraction of the
underlying memory being accessed. While certain languages and applications will
be more susceptible to certain types of logic errors, two of the most common are
bugs are out of bounds accesses and integer overflows and underflows.

## Out of Bounds
Failing to properly check the bounds of an array access, especially when the
index to access is controlled by a potentially malicious user, effectively gives
direct access to memory. Bounds checking remains a very common issue in even
major software products that are used every day. Typically these bugs are
byproducts of not checking the input against the size of the data being accessed
or incorrectly performing such a check. Typically out of bounds accesses are
either a read or a write, but rarely both read and write are possible in the
same bug.

```
                    a[0] a[1] a[3] a[4] ... other_variable
                                     ▲      ▲             
                Normal Access────────┘      │             
                                            │             
                Dangerous Access────────────┘             
```

In the context of native programs we must also remember that strings are just arrays of text variables, often ending in a null byte to designate their end. Strings are a very common culprit of OOB bugs in C and C++ programs due to mistakes in managing how they are copied between buffers.

## Buffer Overflows
Building from out of bounds, we have the similar case of copying too much data
into an undersized buffer without a bounds check. This can be descibed as the
specific mode of linear OOB rather than by-index OOB. Going back to our string
example is probably the most iconic incarnation as C strings are effectively
synonymous with buffer overflow vulnerabilities.

```c
// An example of how easy this is to create
char str1[50] = {};
char *str2 = SOME_GLOBAL;
strcpy(str1, str2);
// We cannot garuntee that str2's size did not just blow past str1's size
```

## Type Overflows
Another concept taught in many computer science courses but often forgotten is
that the bytes allocated for certain types have a maximum value that can be
stored. For example, a `unsigned char` in C can represent any value from `0` to
`255` (`0xff`). However as soon as we add `1` to a char that's already at `255`, that
variable will overflow from `0xff` to `0x0100` which will be truncated back to
`0x00` which is 0. Inversely, if we subtract from a variable that is already at
its lowest possible value, that variable will overflow to its maximum value.

While it seems that this `unsigned char` example is unrealistic for overflows in
commercial software, many applications are optimized for what the developers
think is possible and many safety checks are forgotten or ignored. For a real
world example, just about every MMORPG made has had an overflow exploit in the
mechanics of player attributes or in the market interfaces. When these bugs
exist, they have real world financial value that have an impact on the economy
of those games and can potentially lead to real world monetary value through
item duplication.

## Example Challenge
For an example that combines a simple out of bounds access and an overflow, we
will look at a minified version of a problem from the Hackasat 2 qualifiers.

```c
{{#include examples/logic-error/logic-error.c}}
```

In the above program, we have two important pieces of data. `entries`, a global
array that can be accessed and incremented. And `lock`, a single `unsigned char`
that starts at `0xff` and must be changed to `0x10` to win the challenge.
Importantly, the program also does not validate the index given by the user to
the actual size of the array, giving us an obvious OOB bug. We can use this and
the increment loop to overflow the value of `lock` so that we can win this
challenge.

We can use objdump to discover where `lock` and `entries` are located relative
to one another. This program is simple enough that they are right next to one another without padding:

```
$ objdump -D logic-error | grep -A 10 '<lock>:'
0000000000004010 <lock>:
    4010:       ff

0000000000004011 <entries>:
    4011:       01 02
    4013:       03
    4014:       04 05
```

Due to this layout in memory, we can treat `lock` as the element at position `-1` in `entries`. This lets us win the challenge by repeatedly instructing the program to alter the value at index `-1` (example truncated):

```
$ ./logic-error
0. quit
1. increment
> 1
Select an entry 1-5:
> -1
Entry -1 is now 10

win
```

## A Real World Example
To show how impactful a simple error like this can be, we can examine
[CVE-2021-21225](https://tiszka.com/blog/CVE_2021_21225_exploit.html). This is a
near textbook OOB read that could be elevated into remote code execution (RCE)
against client browsers via the V8 JavaScript engine. Although it involves some
additional techniques in order to get from OOB read to RCE, the simplicity of
the initial bug should show that even the simple bugs are to be taken seriously
in security contexts.