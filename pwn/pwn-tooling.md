# PWN Tooling
These are the go-to tools for pwn, not every problem will need every tool and
not every tool can solve every problem.

There are multiple options here, but getting comfortable with whatever you have
available is usually a safe bet. Examples in this wiki will assume you have a linux environment with ghidra, gdb+gef, gcc, and pwntools installed unless otherwise specified.

Disassemblers / Decompilers
- [Ghidra](https://www.ghidra-sre.org/) ([Installer Tool](https://github.com/christophermorrison/ghidra_installer))
- IDA
- Binary Ninja (binja)

General Utilities
  - [pwntools](/basics/tools/pwntools.md), a binary exploit toolkit
  - Linux binutils package

Debuggers
  - [gdb](/basics/tools/gdb.md) -- Linux machine debugger
    - [gef](https://github.com/hugsy/gef) -- a modern gdb experience
  - LLDB -- LLVM project debugger
  - WinDbg -- the Windows debugger

Instrumentation Tools
  - LIEF
  - LLVM -- fsanitize options

Emulation Tools
  - angr
  - Qemu
  - Qiling
  - Renode.io

Fuzzers
  - AFL++
  - LLVM -- libfuzzer

<!-- TODO: link fuzzing demo slides -->
