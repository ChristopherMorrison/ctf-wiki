Binary Defenses
===============

Shadow Stack / Control-Flow Enforcement
---------------------------------------
Have a second stack to verify your stack
- https://en.wikipedia.org/wiki/Shadow_stack

SMEP/SMAP
---------
Keep the kernel from directly running user-space code.
Keep the kernel from direcly accessing userspace memory under most conditions
- https://wiki.osdev.org/Supervisor_Memory_Protection

RELRO
-----
loader time dlresolve + mprotect R-- GOT, modern windows and linux
