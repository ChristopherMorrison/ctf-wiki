# Intro to CTFs
CTFs are technical challenges, usually related cyber security and
vulnerabilities. Generally CTFs are "jeapoardy style" where teams answer
progressively harder questions from the following categories:

 - Reverse Engineering
 - Binary Exploitation
 - Cryptography
 - Web Exploitation
 - Forensic Analysis

Here are a few of the most popular CTFs each year:
 - [Defcon CTF](https://defcon.org/html/links/dc-ctf.html)
 - [Google CTF](https://capturetheflag.withgoogle.com/)
 - [Hackasat](https://www.hackasat.com/)
 - [CSAW](https://ctf.csaw.io/)
 - [PicoCTF](https://picoctf.org/)

Additional Info / Descriptions
 - https://wiki.bi0s.in/#what-is-a-ctf
 - https://ctf101.org/
