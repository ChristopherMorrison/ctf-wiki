# Contributing
Pages should follow all of the folling points:
- A specific point that the page focuses on (ex SQLi)
- Explanation and examples
- A minimal, easy to run example to showcase

The goal is to have this be a resource that a dedicated reader could follow, or
or an experienced player could teach from.

Would probably be smart to have some ci/cd that checks all the links still 200

Other wikis
 - https://wiki.bi0s.in/
 - https://ctf101.org/
 - https://github.com/christophermorrison/ctf-training -- original version of this, need to merge and condense
 - https://pwn.college/
    - https://www.youtube.com/c/pwncollege/playlists
 - https://ir0nstone.gitbook.io/notes/
 - https://picoctf.org/resources
 - web oriented https://knowledge-base.secureflag.com/vulnerabilities/sql_injection/sql_injection_python.html

Tools
 - disassemblers/decompilers
    - https://cloud.binary.ninja/
    - radare2
    - https://cutter.re/
 - https://github.com/lief-project/LIEF
 - burp suite
 - angr

Meta Lists
 - https://github.com/zardus/ctf-tools
 - https://www.kali.org/tools/
 - https://blogs.nvcc.edu/kdinh/ctftools/
 - https://awesomeopensource.com/projects/ctf-tools
 - https://project-awesome.org/apsdehal/awesome-ctf


Unsorted
 - https://cryptopals.com/
 - gitlab/cyberatuc
 - ctftime.org
 - https://attack.mitre.org/
 - https://twitter.com/sec_r0?s=21
 - https://margin.re/media/an-opinionated-guide-on-how-to-reverse-engineer-software-part-1.aspx
 - https://nora.codes/tutorial/an-intro-to-x86_64-reverse-engineering/
 - https://www.hackasat.com/keep-learning
 - https://ropemporium.com/
